package com.leaobet.people.poker.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class PlayersAdapter(private val players: ArrayList<Players>) :
    RecyclerView.Adapter<PlayersAdapter.ViewHolder>() {

    var playersPosition = 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val playersImg: ImageView = itemView.findViewById(R.id.playerImg)
        val playersName: TextView = itemView.findViewById(R.id.playerName)
        val playersDesc: TextView = itemView.findViewById(R.id.playerDesc)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.best_players_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val playersList = players[position]
        holder.playersDesc.text = playersList.argDesc
        playersList.argPicture?.let { holder.playersImg.setImageResource(it) }
        holder.playersName.text = playersList.argTitle
        playersPosition = holder.adapterPosition
    }

    override fun getItemCount(): Int = players.size
}