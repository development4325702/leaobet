package com.leaobet.people.poker.app

import androidx.appcompat.app.AppCompatActivity
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.leaobet.people.poker.app.databinding.ActivityBestPlayersBinding
class BestPlayersActivity : AppCompatActivity() {

    private lateinit var bestPlayersBinding: ActivityBestPlayersBinding

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bestPlayersBinding = ActivityBestPlayersBinding.inflate(layoutInflater)
        setContentView(bestPlayersBinding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        bestPlayersBinding.backBtn.setOnClickListener {
            onBackPressed()
        }

        val playersData = ArrayList<Players>()
        playersData.add(
            Players(
                "Justin Bonomo",
                R.drawable.image_1,
                "He has been a dominating player in poker and grabbed enormous winnings like no one else. He has conquered the cash winnings worth \$44.6 million and only participates in the high roller tournaments which turn out in incredible wins! He also won the Heads-up-no-limit Hold’em championship and won \$185,965. He has also made his way towards the second bracelet of WSOP which was a great win.\n" +
                        "He has been definitely holding his place among the best poker players in the world."
            )
        )

        playersData.add(
            Players(
                "Bryn Kenney",
                R.drawable.image_2,
                "He has been playing poker game since his school days, which is really incredible! He got to know about the profitability of the game at the age of 16 when he won \$1,00,000 by playing in Super High Roller Tournament at PCA, and he won an amount of \$1,687,800 and also won millions of dollars from the Triton Super High Roller series main event which was held in 2016."
            )
        )

        playersData.add(
            Players(
                "Ben Tollerene",
                R.drawable.image_3,
                "Ben Tolerance definitely has some innate kind of love for the poker game and has given tough competition to the various poker players. The player has always given importance to the high stake tables and has managed to have a good game every time. He has come a really long way which is inspiring for many poker players out there. He started at \$25 buy-ins to make millions in online poker tournaments. In the category of best poker player in the world, he has surely made his position."
            )
        )

        playersData.add(
            Players(
                "Phil Ivey",
                R.drawable.image_4,
                "Talking about the best poker players in the world, the list can not be complete without Phil Ivey. Phil Ivey has been the most feared competitor of the poker game and has literally shown off his skills like no one else. Proving himself in the oﬄine poker games again and again, he has won many hearts by now. Phil has won numerous trophies and medals which are really cherished and admired by a number of people out there. He comes in the list of best poker players. This might be hard to believe, but he already has ten world series of poker titles under his belt which is quite commendable."
            )
        )

        playersData.add(
            Players(
                "Erik Seidel",
                R.drawable.image_5,
                "This 30-year-old New Yorker has been a popular undisputed champion who has been marked by a relatively low profile. He has always given preference to his games and has been doing it for a really long time now. Evidently, he has made his space in the list of best poker players in the world. You can surely see his games and analyze how well he chooses his moves. Surely, he has shown some great tricks in various poker games."
            )
        )

        playersData.add(
            Players(
                "Bertrand Grospellier",
                R.drawable.image_6,
                "As we know, the very popular statement, “Passion pays off” by Bertrand Grospellier is a living example of it. He has made several million in various poker tournaments and also earned two titles that are worth \$500,000. He has studied the game thoroughly and knows how to show it off! He is definitely on the list of best poker players in the world. You can definitely go through his journey and see how well he has done with the game."
            )
        )

        playersData.add(
            Players(
                "Dan Smith",
                R.drawable.image_7,
                "Dan Smith who is one of the best poker players in the world comes from New Jersey. He dropped out of college to make the poker game his career which was pretty risky for anyone! But the player showed his skills and has amazingly made some great records. He is also known as Cowboy Dan, played hard to become a professional poker player at the age of 16 and is genuinely a great poker player. He won his first major tournament in 2008 and also won a \$1,650 Heartland poker tour title for \$101,960. He also went for a massive tear at the EPT Monte Carlo and won three events for \$521,580."
            )
        )

        playersData.add(
            Players(
                "Johnny Moss",
                R.drawable.image_8,
                "He has been known as the Grandfather of Poker and one of the best poker players in the world. He has shown pretty unique tricks with his strategic mind which is really unbelievable for many. He is among those few players who have won the World Series of Poker main event thrice. Also, he won the \$10,000 no-limit Hold’em world championship event, where he also won his WSOP Bracelet for the third time with a cash prize of \$160,000. And, he has also won his fourth bracelet and a cash prize of \$44,000 in the world series of Poker, 6th edition."
            )
        )

        playersData.add(
            Players(
                "Fedor Holz",
                R.drawable.image_9,
                "His name is enough to leave a mark in the poker world. He has created immense popularity with the games he plays. He secured his position in the list of best poker players in the world at the age of 21. He is currently ranked in the fourth position in the Global Poker Index of the top players for live tournaments. He has won over \$26,700,000 and his lifetime winnings are over 32,550,000. Surely, he has taken over many monetary and non-monetary awards which also include the World Series of Poker."
            )
        )

        playersData.add(
            Players(
                "Stephen Chidwick",
                R.drawable.image_10,
                "From being a regular player to becoming one of the best poker players in the world he has come a long way. He has won over \$32,000,000 during his whole poker career which is immensely commendable at such an age. He started playing at the age of 16 and is still in the game with many. He is definitely a tough competitor to the players who are there. The impressive tricks and strategies shown by him have made him ace the game many times in his poker journey."
            )
        )

        playersData.add(
            Players(
                "Phil Hellmuth",
                R.drawable.image_11,
                "Popularly known as the poker brat! He has a huge fan base because of the early-age success he conquered! At the age of 24, he became the youngest world series champion in the WSOP event and also won a gold bracelet. He went to Las Vegas and won a big prize amount of \$10,000 and also walked towards his elite poker career. He is known as the youngest champion in because of his 1989 contest where he secured the winning prize of \$755,000. He has won an enormous number of tournaments like no one else."
            )
        )

        playersData.add(
            Players(
                "Doyle Brunson",
                R.drawable.image_12,
                "When we talk about top poker players in the world he has done it better than anyone else. At the age of 85, he is still playing and participating in some of the huge cash games. He has won 10 bracelets till now by playing in the world series of Poker in the senior category. He is ranked third in the senior category by WSOP. He has retired now after playing poker professionally for more than 50 years. He has made some inspiring moves that have surprised the users."
            )
        )

        bestPlayersBinding.playersRecycler.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        val adapter = PlayersAdapter(playersData)
        bestPlayersBinding.playersRecycler.adapter = adapter

        bestPlayersBinding.nextBtn.setOnClickListener {
            bestPlayersBinding.playersRecycler.postDelayed(Runnable {
                bestPlayersBinding.playersRecycler.smoothScrollToPosition(adapter.playersPosition + 1)
            }, 200)
            adapter.notifyDataSetChanged()
        }
    }
}