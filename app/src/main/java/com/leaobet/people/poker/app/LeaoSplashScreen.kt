package com.leaobet.people.poker.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.leaobet.people.poker.app.databinding.ActivityLeaoSplashScreenBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
class LeaoSplashScreen : AppCompatActivity() {

    private lateinit var splashScreenBinding: ActivityLeaoSplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leao_splash_screen)

        splashScreenBinding = ActivityLeaoSplashScreenBinding.inflate(layoutInflater)
        setContentView(splashScreenBinding.root)
        setLeaoFullScreen()

        GlobalScope.launch {
            delay(2600)
            val intent = Intent(this@LeaoSplashScreen, MainActivity::class.java)
            startActivity(intent)
            finish()
        }


    }

    private fun setLeaoFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}