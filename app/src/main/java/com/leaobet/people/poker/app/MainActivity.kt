package com.leaobet.people.poker.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.leaobet.people.poker.app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        setLeaoFullScreen()

        mainBinding.playersBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, BestPlayersActivity::class.java)
            startActivity(intent)
        }

        mainBinding.exitBtn.setOnClickListener {
            finish()
        }

    }

    private fun setLeaoFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}